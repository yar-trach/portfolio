# PORTFOLIO

## PROFESSIONAL SKILLS

Skill         | Rate (max 5) |  | Skill             | Rate (max 5)
------------- | ------------ |  | ----------------- | ------------
HTML5/CSS3    | 5            |  | LESS/SASS         | 5
Bootstrap 3,4 | 5            |  | JavaScript/jQuery | 4
Grunt/Gulp    | 4            |  | Git               | 4
JSON          | 4            |  | Typo3             | 4
Fluid Typo3   | 3            |  | Docker            | 3
TypoScript    | 3            |  | NPM               | 3
NodeJS        | 2            |  | PHP               | 2
Angular 1,2   | 1            |  | TypeScript        | 1
WordPress     | 1

--------------------------------------------------------------------------------

## PROJECTS

Project                            | Description                                                                            | Technologies
---------------------------------- | -------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------
[Digital Metal][14]                | Creating Docker environment / implementation of design                                 | WordPress / Docker / JavaScript / HTML5 / Bootstrap 3 / SASS / Grunt
[TYPO3 Org (in progress)][1]       | Developing and implementation of FE features / integration FE / creating documentation | Typo3 / JavaScript / HTML5 / Bootstrap 4 / Fluid Typo3 / SASS / TypoScript / Gulp / NPM / Docker
[TYPO3 Extension Repository][2]    | Developing and implementation of FE features / integration FE                          | Typo3 / JavaScript / HTML5 / Bootstrap 4 / Fluid Typo3 / SASS / TypoScript / Gulp / NPM / Docker
[Malmo Arena][3]                   | Developing and implementation of FE features / creating documentation                  | JavaScript / Bootstrap 3 / LESS / HTML5 / Handlebars / Typo3 / Fluid Typo3 / Grunt
[Aimpoint][4]                      | Implementing of new JS features / site and code optimization / creating documentation  | JavaScript / Bootstrap 3 / LESS / HTML5 / Typo3 / Fluid Typo3 / Grunt
[Möllans Ost][5]                   | Developing and implementation of FE features                                           | Typo3 / JavaScript / Bootstrap 3 / LESS / HTML5 / Handlebars / Fluid Typo3 / Grunt / FaceBook API
[Hallberg-Rassy][6]                | Developing and implementation of FE features / site and code optimization              | JavaScript / HTML5 / Bootstrap 3 / LESS / Handlebars / Typo3 / Fluid Typo3 / Grunt
[Oxelösund][7]                     | Implementing of new JS features / site and code optimization / creating documentation  | JavaScript / HTML5 / Bootstrap 3 / LESS / Handlebars / Typo3 / Fluid Typo3 / Grunt
[Hässleholm Miljös][8]             | Redesign / implementation of new FE features                                           | Typo3 / JavaScript / Bootstrap 3 / LESS / Fluid Typo3 / Grunt
[Hässleholm Miljös skolhemsida][9] | Site setup / developing and implementation FE features                                 | Docker / Typo3 / JavaScript / Bootstrap 3 / LESS / HTML5 / Handlebars / Fluid Typo3 / Grunt
[Elisefarm][10]                    | Developing and implementation of FE features                                           | JavaScript / HTML5 / Bootstrap 3 / LESS / Handlebars / Typo3 / Fluid Typo3 / Grunt / FaceBook API
[Nilssonsmotor][11]                | Developing and implementation of FE features                                           | HTML5 / Bootstrap 3 / LESS / JavaScript / Handlebars / Typo3 / Fluid Typo3 / Grunt
[Svenska Motorkompaniet][12]       | Implementing of new JS features / site and code optimization                           | HTML5 / Bootstrap 3 / LESS / JavaScript / Handlebars / Typo3 / Fluid Typo3 / Grunt
[Add Life][13]                     | Implementing of new JS features / site and code optimization                           | HTML5 / Bootstrap 3 / LESS / JavaScript / Handlebars / Typo3 / Fluid Typo3 / Grunt

--------------------------------------------------------------------------------

## Interested in

> - JavaScript
> - CSS3 (LESS/SASS)
> - Creating HTML Markup
> - Creating sites based on TYPO3 / WordPress

[1]: https://stage.typo3.org/
[10]: http://www.elisefarm.se/
[11]: http://nilssonsmotor.typo3konsult.se/
[12]: http://www.smk.se/
[13]: http://www.add.life/
[14]: https://digitalmetal.tech/
[2]: https://extensions.typo3.org/
[3]: http://www.malmoarena.com
[4]: https://www.aimpoint.se/
[5]: http://mollansost.com/
[6]: http://www.hallberg-rassy.com/
[7]: http://www.oxelosund.se/
[8]: http://hassleholmskolsida.typo3konsult.se/
[9]: http://hassleholmskolsida.typo3konsult.se/sopsamlarmonster/
